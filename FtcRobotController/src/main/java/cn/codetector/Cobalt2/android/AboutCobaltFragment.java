package cn.codetector.Cobalt2.android;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qualcomm.ftcrobotcontroller.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class AboutCobaltFragment extends Fragment {

    public AboutCobaltFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_cobalt, container, false);
    }
}
